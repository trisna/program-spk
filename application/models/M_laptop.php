<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_laptop extends CI_Model {

	public $tableName;

	public function __construct(){
		parent::__construct();
		$this->tableName = "tb_laptop";
	}

	public function insert($data){
		$this->db->insert($this->tableName,$data);

		return $this->db->insert_id();
	}

	public function update($id,$data){
		$this->db->set($data);
		$this->db->where('id_laptop',$id);
		$this->db->update($this->tableName);
	}

	public function delete($id){
		$this->db->where('id_laptop',$id);
		$this->db->delete($this->tableName);
	}

	public function selectAll($from=0,$offset=0){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->limit($from,$offset);

		return $this->db->get();
	}

	public function Min($atribut){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->order_by($atribut, "ASC");
		return $this->db->get();
	}

	public function Max($atribut){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->order_by($atribut, "DESC");
		return $this->db->get();
	}

	public function selectById($id){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('id_laptop',$id);		

		return $this->db->get();
	}

	public function selectByToken($token){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('token',$token);		

		return $this->db->get();
	}

	public function selectBylaptopname($laptopname){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('laptopname',$laptopname);
		

		return $this->db->get();
	}
	
	public function checkLogin($laptopname,$pwd){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('laptopname',$laptopname);
		$this->db->where('password',$pwd);

		if($this->db->get()->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	
}
