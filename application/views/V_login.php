<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SIRELA</title>
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- Animate.css -->
  <link href="<?php echo base_url(); ?>assets/vendors/animate.css/animate.min.css" rel="stylesheet">

  
  <style>
  /* The Modal (background) */
  .modal{
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 20px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }

  /* Modal Content */
  .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px 50px;
    border: 1px solid #888;
    width: 50%;
  }

  /* The Close Button */
  .close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }

  input[type=submit], .form-style-1 input[type=button]{
    margin-left: 20px;
    background: #4169E1;
    padding: 8px 15px 8px 15px;
    border: none;
    color: #fff;
  }


  input[type=submit]:hover, .form-style-1 input[type=button]:hover{
    background: #4691A4;
    box-shadow:none;
    -moz-box-shadow:none;
    -webkit-box-shadow:none;
  }
</style>
</head>

<body>
  <!-- Trigger/Open The Modal -->
  <input id="myBtn" type="submit" value="LOGIN"/>

  <!-- The Modal -->
  <div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
      <span class="close">&times;</span>
      <h3><b>Login Form</b></h3>
      <hr>
      <br>
      <center>
        <form action="<?php echo site_url('C_login/loginAct');?>" class="form-horizontal" role="form" method="POST">
          <div class="form-group">
            <div class="col-sm-2">
              <label for="inputEmail3" class="control-label">Username</label>
            </div>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="username" placeholder="Username">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label for="inputPassword3" class="control-label">Password</label>
            </div>
            <div class="col-sm-10">
              <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
          </div>              
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button  style="background: #B0E0E6" type="submit" class="btn btn-default">Sign in</button>
            </div>
          </div>
        </form>

      </center>
    </div>

  </div>

  <?php 



if($this->session->flashdata('jalankan')){

  $processor = (int)$processor['processor']; 
  $vga = (int)$vga['vga']; 
  $ram = (int)$ram['ram']; 
  $hardisk = (int)$hardisk['hardisk']; 
  $harga = (int)$harga['harga']; 
  
}
/*echo $processor.'---';
echo $vga.'---';
echo $ram.'---';
echo $hardisk.'---';
echo $harga.'---';*/

?>
<?php 
if($this->session->flashdata('jalankan')){

  $no=1;
  $jumlah_temp=0;
  foreach ($datalaptop as $key) {
      //echo "$processor";


    $processor_temp = ((int)$key['processor'] / $processor) * $databobot['processor']; 
    $vga_temp = ((int)$key['vga'] / $vga) * $databobot['vga']; 
    $ram_temp = ((int)$key['ram'] / $ram) * $databobot['ram']; 
    $hardisk_temp = ((int)$key['hardisk'] / $hardisk) * $databobot['hardisk']; 
    $harga_temp = ($harga / (int)$key['harga']) * $databobot['harga'];
    $Jumlah = $processor_temp + $vga_temp + $ram_temp + $hardisk_temp +$harga_temp;

    if($jumlah_temp <= $Jumlah){
      $id = $key['id_laptop'];
      $jumlah_temp = $Jumlah;
    }

  }

}

?>
<h1>SI<span class="colour">RELA</span></h1>
<h4>Sistem Rekomendasi Laptop</h4>

<div class="grid" >

  <div class="box box1"><div>
    <div class="gallery">



      <a target="_blank" >
        <!-- Motor Jepang -->
        <?php 
        if($this->session->flashdata('jalankan')){
          $no=1;
          $jumlah_temp=0;
          foreach ($datalaptop as $key) {
            if($id == $key['id_laptop']){
              ?>
              <img src="<?php echo base_url(); ?>images/<?php echo $key['foto']; ?>"alt="Trolltunga Norway" width="600" height="400">
              <div class="desc">
                <b><?php echo $key['nama'] ?></b><br><br>
                RAM : <?php echo $key['ram'] ?><br>
                VGA : <?php echo $key['vga'] ?><br>
                Hardisk : <?php echo $key['hardisk'] ?><br>
                Harga : <?php echo $key['harga'] ?><br>

              </div>
            <?php }} 
          }

          ?>
        </a>



      </div>
    </div>
  </div>

  <div class="box box2"><div>

    <?php if($this->session->flashdata('jalankan')){ ?>
      <form method="post" action="<?php echo site_url('C_login/bobot'); ?>" class="form-horizontal" role="form" onsubmit="return validasi_input(this)">
        <ul class="form-style-1">            

          <li>
            <label>Core <span class="required">*</span></label>
            <select name="jenisMotor" class="field-select">
              <option value="nonjapan">I3</option>
              <option value="japan">I5</option>
              <option value="nonjapan">I7</option>
            </select>
          </li>

          <li>
            <label>Processor <span class="required">*</span></label>
            <input onkeypress="return hanyaAngka(event)" type="text" name="c1" value="<?php echo $databobot['processor']*100; ?>" class="field-divided" placeholder=""/>
          </li>


          <li>
            <label>RAM <span class="required">*</span></label>
            <input onkeypress="return hanyaAngka(event)" type="text" name="c2" value="<?php echo $databobot['ram']*100; ?>" class="field-divided" placeholder=""/>
          </li>

          <li>
            <label>Hardisk <span class="required">*</span></label>
            <input onkeypress="return hanyaAngka(event)" type="text" name="c3" value="<?php echo $databobot['hardisk']*100; ?>" class="field-divided" placeholder=""/>
          </li>

          <li>
            <label>VGA <span class="required">*</span></label>
            <input onkeypress="return hanyaAngka(event)" type="text" name="c4" value="<?php echo $databobot['vga']*100; ?>" class="field-divided" placeholder=""/>
          </li>

          <li>
            <label>Harga <span class="required">*</span></label>
            <input onkeypress="return hanyaAngka(event)" type="text" name="c5" value="<?php echo $databobot['harga']*100; ?>" class="field-divided" placeholder=""/>
          </li>

          <li>
            <input onclick="myFunction()" style="background: #4169E1" type="submit" value="Cari Rekomendasi" name="btnKirim" />
          </li>




        </form>
      <?php }else{ ?>
        <form method="post" action="<?php echo site_url('C_login/bobot'); ?>" class="form-horizontal" role="form" onsubmit="return validasi_input(this)">
          <ul class="form-style-1">            

            <li>
              <label>Core <span class="required">*</span></label>
              <select name="jenisMotor" class="field-select">
                <option value="nonjapan">I3</option>
                <option value="japan">I5</option>
                <option value="nonjapan">I7</option>
              </select>
            </li>

            <li>
              <label>Processor <span class="required">*</span></label>
              <input onkeypress="return hanyaAngka(event)" type="text" name="c1"  class="field-divided" placeholder=""/>
            </li>


            <li>
              <label>RAM <span class="required">*</span></label>
              <input onkeypress="return hanyaAngka(event)" type="text" name="c2"  class="field-divided" placeholder=""/>
            </li>

            <li>
              <label>Hardisk <span class="required">*</span></label>
              <input onkeypress="return hanyaAngka(event)" type="text" name="c3"  class="field-divided" placeholder=""/>
            </li>

            <li>
              <label>VGA <span class="required">*</span></label>
              <input onkeypress="return hanyaAngka(event)" type="text" name="c4"  class="field-divided" placeholder=""/>
            </li>

            <li>
              <label>Harga <span class="required">*</span></label>
              <input onkeypress="return hanyaAngka(event)" type="text" name="c5"  class="field-divided" placeholder=""/>
            </li>

            <li>
              <input onclick="myFunction()" style="background: #4169E1" type="submit" value="Cari Rekomendasi" name="btnKirim" />
            </li>




          </form>
        <?php }?>
      </div>
    </div>


  </div>



  <center><button onclick="myFunction()" style="background: #4169E1" >Details</button></center>

  <div id="myDIV" style="display: none;"> 
    <h3>TABEL DATA AWAL</h3>
    <table align="center" id="customers">
      <tr >
        <th style="background: #4169E1">Number</th>
        <th style="background: #4169E1">Code</th>
        <th style="background: #4169E1">Name</th>
        <th style="background: #4169E1">Processor</th>
        <th style="background: #4169E1">RAM</th>
        <th style="background: #4169E1">Hardisk</th>
        <th style="background: #4169E1">VGA</th>
        <th style="background: #4169E1">Price</th>
      </tr>
      <?php if($this->session->flashdata('jalankan')){ ?>
        <?php 
        $no=1;
        foreach ($datalaptop as $key) {
          ?>
          <tr>

            <td class=" "><?php echo $no++; ?></td>
            <td class=" "><?php echo $key['code']; ?></td>
            <td class=" "><?php echo $key['nama']; ?></td>
            <td class=" "><?php echo $key['processor']; ?></td>
            <td class=" "><?php echo $key['ram']; ?></td>
            <td class=" "><?php echo $key['hardisk']; ?></td>
            <td class=" "><?php echo $key['vga']; ?></td>
            <td class=" "><?php echo $key['harga']; ?></td>

          </tr>
        <?php } ?>
      <?php }?>

    </table>

    <h3>TABEL DATA SETELAH PROSES PEMBAGIAN NILAI MAX/MIN</h3>
    <table align="center" id="customers">
      <tr>
        <th style="background: #4169E1">Number</th>
        <th style="background: #4169E1">Code</th>
        <th style="background: #4169E1">Name</th>
        <th style="background: #4169E1">Processor</th>
        <th style="background: #4169E1">RAM</th>
        <th style="background: #4169E1">Hardisk</th>
        <th style="background: #4169E1">VGA</th>
        <th style="background: #4169E1">Price</th>
      </tr>
      <?php if($this->session->flashdata('jalankan')){ ?>
        <?php 
        $no=1;

        foreach ($datalaptop as $key) {
      //echo "$processor";
          $processor_temp = (int)$key['processor'] / $processor; 
          $vga_temp = (int)$key['vga'] / $vga; 
          $ram_temp = (int)$key['ram'] / $ram; 
          $hardisk_temp = (int)$key['hardisk'] / $hardisk; 
          $harga_temp = $harga / (int)$key['harga'];

          ?>
          <tr>

            <td class=" "><?php echo $no++; ?></td>
            <td class=" "><?php echo $key['code']; ?></td>
            <td class=" "><?php echo $key['nama']; ?></td>
            <td class=" "><?php echo $processor_temp; ?></td>
            <td class=" "><?php echo $ram_temp; ?></td>
            <td class=" "><?php echo $hardisk_temp; ?></td>
            <td class=" "><?php echo $vga_temp; ?></td>
            <td class=" "><?php echo $harga_temp; ?></td>

          </tr>
        <?php } ?>

      <?php }?>
    </table>

    <h3>TABEL DATA SETELAH PROSES PERKALIAN NILAI BOBOT</h3>
    <table align="center" id="customers">
      <tr>
        <th style="background: #4169E1">Number</th>
        <th style="background: #4169E1">Code</th>
        <th style="background: #4169E1">Name</th>
        <th style="background: #4169E1">Processor</th>
        <th style="background: #4169E1">RAM</th>
        <th style="background: #4169E1">Hardisk</th>
        <th style="background: #4169E1">VGA</th>
        <th style="background: #4169E1">Harga</th>
        <th style="background: #4169E1">Jumlah</th>
      </tr>

      <?php if($this->session->flashdata('jalankan')){ ?>
        <?php 

        $no=1;
        foreach ($datalaptop as $key) {
      //echo "$processor";
          $processor_temp = ((int)$key['processor'] / $processor) * $databobot['processor']; 
          $vga_temp = ((int)$key['vga'] / $vga) * $databobot['vga']; 
          $ram_temp = ((int)$key['ram'] / $ram) * $databobot['ram']; 
          $hardisk_temp = ((int)$key['hardisk'] / $hardisk) * $databobot['hardisk']; 
          $harga_temp = ($harga / (int)$key['harga']) * $databobot['harga'];
          $Jumlah = $processor_temp + $vga_temp + $ram_temp + $hardisk_temp +$harga_temp;
          ?>
          <tr>

            <td class=" "><?php echo $no++; ?></td>
            <td class=" "><?php echo $key['code']; ?></td>
            <td class=" "><?php echo $key['nama']; ?></td>
            <td class=" "><?php echo $processor_temp; ?></td>
            <td class=" "><?php echo $ram_temp; ?></td>
            <td class=" "><?php echo $hardisk_temp; ?></td>
            <td class=" "><?php echo $vga_temp; ?></td>
            <td class=" "><?php echo $harga_temp; ?></td>

            <td class=" "><?php echo $Jumlah; ?></td>

          </tr>
        <?php } ?>
      <?php }?>



    </table>

  </div>



  <script>
    function myFunction() {
      var x = document.getElementById("myDIV");
      if (x.style.display === "none") {
        x.style.display = "block";
      } else {
        x.style.display = "none";
      }
    }
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }

    function validasi_input(form){

      var tinggi = parseInt(form.tinggiBadan.value);

      if (tinggi < 152.5 || tinggi > 177.5 ){
        alert("Input tinggi badan harus antara 152.5 - 177.5");
        return (false);
      }

      if (form.tinggiBadan.value == ""){
        alert("Tinggi badan masih kosong!");
        form.tinggiBadan.focus();
        return (false);
      }

      if (form.c1.value == ""){
        alert("Bobot Processor masih kosong!");
        form.c1.focus();
        return (false);
      }

      if (form.c2.value == ""){
        alert("Bobot RAM masih kosong!");
        form.c2.focus();
        return (false);
      }

      if (form.c3.value == ""){
        alert("Bobot Hardisk masih kosong!");
        form.c3.focus();
        return (false);
      }

      if (form.c4.value == ""){
        alert("Bobot VGA masih kosong!");
        form.c4.focus();
        return (false);
      }

      if (form.c5.value == ""){
        alert("Bobot prize masih kosong!");
        form.c5.focus();
        return (false);
      }

      var c1 = form.c1.value;
      var c2 = form.c2.value;
      var c3 = form.c3.value;
      var c4 = form.c4.value;
      var c5 = form.c5.value;

      var result = parseInt(c1) + parseInt(c2) + parseInt(c3) + parseInt(c4) + parseInt(c5);
      if (result != 100){
        alert("Jumlah bobot harus 100%");
        return (false);
      }


      return (true);
    }

    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function() {
      modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }


  </script>



</body>

</html> 