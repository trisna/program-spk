<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3> Laptop <small></small></h3>
      </div>

    </div>

    <!-- row -->
    <div class="row">

      <!--  CONTENT SPACE! -->
      <div class="x_panel">
        <div class="x_title">     
          <h2>Data Laptop Intel i5</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
              <a href="#add" data-toggle="modal"><button class="btn btn-info btn-xs" ><span class="fa fa-pencil" ></span> Tambah Data </button></a>
            </li>
          </ul>



          <div class="clearfix"></div>
        </div>

        <div class="x_content">

          <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action">
              <thead>
                <tr class="headings">
                  <th>Number</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Processor</th>
                  <th>RAM</th>
                  <th>Hardisk</th>
                  <th>VGA</th>
                  <th>Price</th>
                 
                  <th style="width: 20%" class="column-title no-link last"><span class="nobr">Action</span></th>
                
                </tr>
              </thead>

              <tbody>

                <?php $no=1;
                 // var_dump($datalaptop); echo "$no---";
                foreach ($datalaptop as $key) {
                //var_dump($datalaptop);

                  # code...
               ?>
                <tr class="even pointer">

                  <td class=" "><?php echo $no; ?></td>
                  <td class=" "><?php echo $key['code']; ?></td>
                  <td class=" "><?php echo $key['nama']; ?></td>
                  <td class=" "><?php echo $key['processor']; ?></td>
                  <td class=" "><?php echo $key['ram']; ?></td>
                  <td class=" "><?php echo $key['hardisk']; ?></td>
                  <td class=" "><?php echo $key['vga']; ?></td>
                  <td class=" "><?php echo $key['harga']; ?></td>
                  
                  <td>
                   
                    <a href="<?php echo site_url('C_admin/laptopAct/dell/'.$key['id_laptop']); ?>" data-toggle="modal"><button class="btn btn-danger btn-xs" ><span class="fa fa-eye" aria-hidden="true"></span> Delete </button></a>         
                  </td>

                </tr>

              <?php $no++; } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>




    </div>
    <!-- /row -->

  </div>
</div>
<!-- /page content -->
<?php $kode = ((int)$key['id_laptop'])+1; $kode = $kode - 3; ?>
<div id="add" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <?php echo form_open_multipart('C_admin/laptopAct/add');?>
      <div class="modal-header">            
        <h4 class="modal-title">Add Form</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">     
       <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" class="form-control" required>
      </div>     
      <div class="form-group">
        <label>Processor</label>
        <input type="number" name="processor" class="form-control" required>
        <input type="text" name="code" value="L_I5_<?php echo $kode; ?>"  hidden="">
        <input type="text" name="core" value="i5"  hidden="">
      </div>
      <div class="form-group">
        <label>RAM</label>
        <input type="number" name="ram" class="form-control" required>
      </div>
      <div class="form-group">
        <label>Hardisk</label>              
        <input type="number" name="hardisk" class="form-control">                                        
      </div>
      <div class="form-group">
        <label>VGA</label>
        <input type="number" name="vga" class="form-control">            
      </div>  
      <div class="form-group">
        <label>Harga</label>
        <input type="number" name="harga" class="form-control">            
      </div>
      <div class="form-group">
        <label>Foto</label>            
        <input type="file" data-role="magic-overlay" name="filefoto" data-edit="insertImage" />
      </div>          
    </div>
    <div class="modal-footer">
      <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
      <input id="tombol" type="submit" class="btn btn-success" value="Add">
    </div>
  </form>
</div>
</div>
</div>


<div id="batalModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">            
          <h4 class="modal-title">Delete</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">          
          Apakah Anda yakin akan menghapus data laptop ini? 
          <br>
          <br>
          
        </div>
        <div class="modal-footer">
          <input type="button" class="btn btn-default" data-dismiss="modal" value="Kembali">
          <input id="tombol" type="submit" class="btn btn-success" value="Ya">
        </div>
      </form>
    </div>
  </div>
</div>