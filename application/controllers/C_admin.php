<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_admin extends CI_Controller {

	
	public function __construct(){
		parent::__construct();
		$this->head['datauser'] = $this->M_user->selectById($_SESSION['id_user'])->row_array();		
		//$this->head['segment'] = $this->uri->segment(1);
		//$this->head['segment'] = $this->uri->segment(2);
		//$test = $_SESSION['id_pegawai'];
		//var_dump($this->head);
		//var_dump($test);
		

		//$this->load->library('pdf');
	}

	public function laptop(){	

		$data['datalaptop'] = $this->M_laptop->selectAll()->result_array();

		$this->load->view('V_header', $this->head);
		$this->load->view('v_i5', $data);
		$this->load->view('V_footer');		
	}


	public function laptopAct($input, $id=0){
		if($input == 'add'){

			$config['upload_path'] = './images/'; //path folder
			$config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan

			$config['file_name'] = true;       	        

			$this->load->library('upload',$config);
			$this->upload->initialize($config);


			if(!empty($_FILES['filefoto']['name'])){
				if(!$this->upload->do_upload('filefoto')){
					$this->upload->display_errors();  
				}else{
					$foto = $this->upload->data('file_name');
				}
			}

			$data= array(

				'code' => $this->input->post('code'),
				'nama' => $this->input->post('nama'),
				'core' => $this->input->post('core'),
				'processor' => $this->input->post('processor'),
				'ram' => $this->input->post('ram'),
				'hardisk' => $this->input->post('hardisk'),
				'vga' => $this->input->post('vga'),
				'harga' => $this->input->post('harga'),
				'foto' => $foto
				
			);

			$this->M_laptop->insert($data);

		}else if($input == "dell"){

			$this->M_laptop->delete($id);
		}		
		

		redirect('C_admin/laptop');
	}





}
