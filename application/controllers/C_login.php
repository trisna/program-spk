<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		if(!$this->session->userdata('logged_in')) {
			$data['datalaptop'] = $this->M_laptop->selectAll()->result_array();
			$data['processor'] = $this->M_laptop->Max('processor')->row_array();
			$data['ram'] = $this->M_laptop->Max('ram')->row_array();
			$data['vga'] = $this->M_laptop->Max('vga')->row_array();
			$data['hardisk'] = $this->M_laptop->Max('hardisk')->row_array();
			$data['harga'] = $this->M_laptop->Min('harga')->row_array();
			//var_dump($data['processor']);
			$this->load->view('V_login', $data);
		}else{
			redirect('C_admin');
		}
	}

	public function bobot(){
		$data['databobot'] = array(
			"processor" => (int)$this->input->post('c1')/100,
			"ram" => (int)$this->input->post('c2')/100,
			"vga" => (int)$this->input->post('c3')/100,
			"hardisk" => (int)$this->input->post('c4')/100,
			"harga" => (int)$this->input->post('c5')/100
		);

		$this->session->set_flashdata("jalankan","Username atau Password Salah!!!");

		$data['datalaptop'] = $this->M_laptop->selectAll()->result_array();
		$data['processor'] = $this->M_laptop->Max('processor')->row_array();
		$data['ram'] = $this->M_laptop->Max('ram')->row_array();
		$data['vga'] = $this->M_laptop->Max('vga')->row_array();
		$data['hardisk'] = $this->M_laptop->Max('hardisk')->row_array();
		$data['harga'] = $this->M_laptop->Min('harga')->row_array();
			//var_dump($data['processor']);
		$this->load->view('V_login', $data);		
	}


	public function loginAct(){
		print_r($this->input->post());
		//var_dump($this->input->post('username'));
		if($this->M_user->checkLogin($this->input->post('username'),($this->input->post('password')))){
			$data = $this->M_user->selectByUsername($this->input->post('username'))->row_array();
			$userdata = array(
				'id_user'  => $data['id_user'],
				'nama_lengkap' =>$data['nama_lengkap'],
				'username'  => $data['username'],
				'logged_in' => true
			);



			$this->session->set_userdata($userdata);
			redirect('C_admin/laptop');
			
			
		}else{
			$this->session->set_flashdata("message","Username atau Password Salah!!!");
			redirect('/');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('C_login');
	}

}
